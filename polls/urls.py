from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^person/(?P<person_id>[0-9]+)/$', views.person, name='person'),
    url(r'^movie/(?P<movie_id>[0-9]+)/$', views.movie, name='movie'),
    url(r'^movies/$', views.movies, name='movies'),
    url(r'^people/$', views.people, name='people'),
    url(r'^user/$', views.user, name='user'),
    url(r'^auth/$', views.auth, name='auth'),
]