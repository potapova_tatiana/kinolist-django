from django.contrib import admin

from .models import User, Movie, Person, PersonMovie, UserMovie

admin.site.register(User)
admin.site.register(Movie)
admin.site.register(Person)
admin.site.register(PersonMovie)
admin.site.register(UserMovie)