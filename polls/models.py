from django.db import models
from django.db.models import Avg

class Person(models.Model):
    name = models.CharField(max_length=50)
    second_name = models.CharField(max_length=50)
    birthday = models.DateField()
    country = models.CharField(max_length=50)
    def __str__(self):
        return self.name + " " + self.second_name

class User(models.Model):
    login = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    def __str__(self):
        return self.login + " " + self.email

class Movie(models.Model):
    name = models.CharField(max_length=50)
    genre = models.CharField(max_length=50)
    time = models.IntegerField(default=0)
    year = models.IntegerField(default=0)
    def __str__(self):
        return self.name
    def user_avg(self):
        return UserMovie.objects.filter(idmovie=self).aggregate(Avg('mark'))['mark__avg']

class PersonMovie(models.Model):
    idactor = models.ForeignKey(Person, on_delete=models.CASCADE)
    idmovie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    role = models.CharField(max_length=50)
    def __str__(self):
        return self.idactor.name + " " + self.idactor.second_name + " " + self.idmovie.name

class UserMovie(models.Model):
    iduser = models.ForeignKey(User, on_delete=models.CASCADE)
    idmovie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    mark = models.IntegerField(default=0)
    def __str__(self):
        return self.iduser.login + " " + self.iduser.email + " " + self.idmovie.name