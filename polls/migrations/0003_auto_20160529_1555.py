# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-05-29 12:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0002_auto_20160529_1354'),
    ]

    operations = [
        migrations.AddField(
            model_name='movie',
            name='image',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='person',
            name='image',
            field=models.CharField(default='', max_length=50),
        ),
    ]
