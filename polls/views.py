from django.shortcuts import get_object_or_404, render

from .models import Movie, Person, User, PersonMovie, UserMovie

def index(request):
    latest_movie_list = Movie.objects.order_by('-id')[:3]
    context = {'latest_movie_list': latest_movie_list}
    return render(request, 'polls/index.html', context)

def movies(request):
    movie_list = Movie.objects.all()
    context = {'movie_list': movie_list}
    return render(request, 'polls/movies.html', context)

def people(request):
    people_list = Person.objects.all()
    context = {'people_list': people_list}
    return render(request, 'polls/people.html', context)

def person(request, person_id):
    person = get_object_or_404(Person, pk=person_id)
    return render(request, 'polls/person.html', {'person': person})

def movie(request, movie_id):
    movie = get_object_or_404(Movie, pk=movie_id)
    return render(request, 'polls/movie.html', {'movie': movie})

def user(request):
    user = get_object_or_404(User, login=request.POST['param1'], password=request.POST['param2'])
    movie_list = Movie.objects.all()
    return render(request, 'polls/user.html', {'user': user, 'movie_list':movie_list})

def user(request):
    if request.POST.get('param1') and request.POST.get('param2'):
        user = get_object_or_404(User, login=request.POST['param1'], password=request.POST['param2'])
    if request.POST.get('param3'):
        movie = get_object_or_404(Movie, name=request.POST['param3'])
    if request.POST.get('param4'):
        q = UserMovie.objects.create(iduser=user, idmovie=movie, mark=request.POST['param4'])
    movie_list = Movie.objects.all()
    return render(request, 'polls/user.html', {'user': user, 'movie_list':movie_list})

def auth(request):
    return render(request, 'polls/auth.html', {})